org 7c00h

	jmp 0:setup			; Far jump -- set CS to 0, IP to 7c02
setup:
	xor ax, ax			; Set segments to known values
	mov sp, ax
	mov ss, ax
	mov ds, ax
	mov ax, 0b800h
	mov es, ax

	mov al, 3			; Set VGA mode 80x25
	int 10h

	cld
	mov si, text1
	xor di, di			; Position -- 0, left upper corner
	mov ah, 74h			; Set font
	call print
	jmp $				; while(1) {}

place:	stosw
print:
	lodsb				; Load character to AL
	and al, al			; Check if there is non-NULL character
	jnz place			; Write it to VRAM together with attribute byte
	ret

text1:
	db "LOL U MAD?", 0

	times 512 - 2 - ($-$$) db 0	; Necessary padding
	db 055h, 0aah
