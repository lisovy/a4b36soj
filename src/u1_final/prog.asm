org 7c00h

	push dx
	push cx
	push bx
	push ax
	pushf
	push ss
	push es
	push ds
	call 0:csip			; Push CS and IP
csip:	push sp
	push bp
	push di
	push si

	jmp 0:setup			; Far jump -- set CS to 0, IP to "setup" label
setup:
	xor ax, ax			; Set segments to known values
	mov ds, ax
	mov ax, 0xB800
	mov es, ax

	mov al, 3			; Set VGA mode 80x25
	int 10h

	cld
	mov si, regs_txt
	xor di, di			; Position -- 0, left upper corner
	mov ah, 0x9			; Set text attribute
	mov cx, 1			; Counter -- which reg will be printed
	jmp print

place:
	stosw

	cmp al, ':'
	jnz print

	pop dx
	cmp cx, 4			; We are printing SP
	jz .spmodify

	cmp cx, 5			; We are printing IP
	jz .ipmodify

	jnz .convert_to_ascii

.spmodify:
	add dx, 10*2
	jmp .convert_to_ascii

.ipmodify:
	sub dx, csip - $$
	jmp .convert_to_ascii

.convert_to_ascii:
	mov al, dh
	shr al, 4
	call putc_hexa

	mov al, dh
	and al, 0xf
	call putc_hexa

	mov al, dl
	shr al, 4
	call putc_hexa

	mov al, dl
	and al, 0xf
	call putc_hexa

	mov al, ' '
	stosw

	inc cx
	mov ax, 0x0900
	mul cx				; Set text attribute

print:
	lodsb				; Load character to AL
	test al, al			; Check if there is non-NULL character
	jnz place			; Write it to VRAM together with attribute byte

clear_scr:
	mov ax, 0
	cmp di, 25*80
	jz end
	stosw
	jmp clear_scr
end:
	jmp $				; while(1) {}



putc_hexa:				; [ES:DI] <- AL in ASCII hexa
	cmp al, 10			; http://www.df.lth.se/~john_e/gems/gem003a.html
	sbb al, 0x69			; - "" -
	das				; - "" -
	stosw

	ret

regs_txt:
	db "SI:DI:BP:SP:IP:CS:DS:ES:SS:FL:AX:BX:CX:DX:", 0

	times 512 - 2 - ($-$$) db 0	; Necessary padding
	db 055h, 0aah
