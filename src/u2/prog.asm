org 7c00h

	jmp 0:start			; Far jump -- set CS to 0, IP to 7c02
start:
	xor ax, ax			; Set segments to known values
	mov ds, ax			; FIXME is this correct?
	mov ax, 0xB800
	mov es, ax

	mov ax, 3			; Set VGA mode 80x25
	int 10h

	mov ax, 0x1112			; Switch to 80x50 mode
	int 10h

	mov ax, 0			; Clear screen
	mov cx, 80*50
	mov di, 0
	mov si, 0
	rep stosw

	; Register an interrupt handler for the keyboard
	cli
	mov word [9*4], kbd_isr		; irq1 -> int9
	mov word [9*4 + 2], 0		; set CS
	sti

%define COUNTER_SIZE			10
print_counter:
	std				; Decrement DI on stosw
	mov cx, COUNTER_SIZE		; For each digit
	mov di, (80 + 80 - 2) * 2
	mov si, counter + COUNTER_SIZE - 1
	mov ah, 0x1E			; Character attribute

print_digit:
	mov al, [si]
	cmp bp, 1
	jne .noincrement

	cmp al, '9'
	jne .nooverflow

	mov al, '0'
	mov [si], al
	mov bp, 1
	jmp .writedigit

.nooverflow:
	add al, 1
	mov [si], al
	xor bp, bp

.writedigit:
.noincrement:
	stosw
	sub si, 1
	loop print_digit

	; Increment the counter
	mov bp, 1
	jmp print_counter

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; DH = VGA attribute
; DL = Integer to print
; DI = video ram address (with offset)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_draw_character:
	push ax
	push cx
	push bx
	push dx

	mov dl, 0
	call _draw_line				; Padding

	pop dx
	xor ax, ax
	mov al, dl
						; FIXME I do not like this part
	mov bx, font
	add bx, ax				; += DL
	shl ax, 2				;
	add bx, ax				; += 4*DL
	mov dl, [bx]

	mov cx, 5
.dl:	call _draw_line
	add bx, 1
	mov dl, [bx]
	loop .dl

	mov dl, 0
	call _draw_line				; Padding

	pop bx
	pop cx
	pop ax
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; DH = VGA attribute
; DL = 8b mask defining line
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_draw_line:
	push cx
	push ax

	mov cx, 8			; 8 Columns per line

.nextcol:
	mov ah, dh
	mov al, ' '
	rol dl, 1			; Fill CF with MSB
	jnc .blankchar
	add al, 0xdb - ' '
.blankchar:
	stosw
	loop .nextcol

	add di, ((80 - 8) * 2) 		; Move to the next line in the VGA text mode

	pop ax
	pop cx
	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
kbd_isr:
	pushf
	push ax
	push bx
	push cx
	push dx
	push es
	push ds
	push si
	push di
	push bp

	cld				; Increment direction: +

	mov ax, 0x0			; Set segments to known values
	mov ds, ax
	mov ax, 0xB800
	mov es, ax

	mov ax, 33333			; N_new = M*N_old + A; M=33333, A=1
	mul word [pseudorand_N]
	add ax, 1
	mov [pseudorand_N], ax
					;;; [AX = N_new]

	mov dx, 0
	mov bx, 65			; Calculate X, Y position
	div bx				; X = N_new % 65
					;;; [AX = N_new/65]
					;;; [DX = N_new%65]

	mov di, 0
	add di, dx
	add di, dx			; DI = 2*X

	mov dx, 0			; Y = (N_new / 65) % 44
	mov bx, 44
	div bx
					;;; [AX = (N_new/65)/44]
					;;; [DX = (N_new/65)%44]

	mov ax, 80*2
	mul dx
	add di, ax			; DI += 80*2*Y


	in al, 60h			; Read the scancode

	mov dh, al
	mov dl, al
	shr dh, 4
	and dl, 0xf
	cmp dh, dl			; Is the low and high nibble of the
					; scancode the same?
	jne .no_change_to_attribute

	not dl				; Invert the Ink nibble
	and dl, 0xf
	shl dh, 4
	or dh, dl
	jmp cont1

.no_change_to_attribute:
	mov dh, al			; Used for attribute
cont1:	mov dl, al			; Used as nuber to print
	shr dl, 4			; Use high nibble
	call _draw_character

	sub di, 7*80 * 2 - 16		; Draw second number next to the first one
	mov dl, al
	and dl, 0xf			; Use low nibble
	call _draw_character

	mov al, 0x20			; Disable interrupt in the PIC
	out 20h, al

	pop bp
	pop di
	pop si
	pop ds
	pop es
	pop dx
	pop cx
	pop bx
	pop ax
	popf
	iret

counter:
	db '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'

pseudorand_N:
	dw 0x0, 0x0

font:
	db 01111110b
	db 01000010b
	db 01000110b
	db 01000110b
	db 01111110b

	db 00001000b
	db 00001000b
	db 00011000b
	db 00011000b
	db 00011000b

	db 01111110b
	db 00000010b
	db 01111110b
	db 01100000b
	db 01111110b

	db 01111110b
	db 00000010b
	db 01111110b
	db 00000110b
	db 01111110b

	db 01100010b
	db 01100010b
	db 01100010b
	db 01111110b
	db 00000010b

	db 01111110b
	db 01000000b
	db 01111110b
	db 00000110b
	db 01111110b

	db 01111110b
	db 01000000b
	db 01111110b
	db 01100010b
	db 01111110b

	db 01111110b
	db 00000010b
	db 00000110b
	db 00000110b
	db 00000110b

	db 00111100b
	db 00100100b
	db 01111110b
	db 01000110b
	db 01111110b

	db 01111110b
	db 01100010b
	db 01111110b
	db 00000010b
	db 01111110b

	db 01111110b
	db 01000010b
	db 01000110b
	db 01111110b
	db 01000110b

	db 01000000b
	db 01111110b
	db 01000110b
	db 01000110b
	db 01111110b

	db 01111110b
	db 01000000b
	db 01100000b
	db 01100000b
	db 01111110b

	db 00000010b
	db 01111110b
	db 01100010b
	db 01100010b
	db 01111110b

	db 01111110b
	db 01000110b
	db 01111110b
	db 01000000b
	db 01111110b

	db 01111110b
	db 01000000b
	db 01111110b
	db 01100000b
	db 01100000b

	times 512 - 2 - ($-$$) db 0	; Necessary padding
	db 055h, 0aah
