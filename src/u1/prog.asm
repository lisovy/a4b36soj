org 7c00h

	jmp 0:setup			; Far jump -- set CS to 0, IP to 7c02
setup:
	xor ax, ax			; Set segments to known values
	mov sp, ax
	mov ss, ax

	mov ax, 0b800h
	mov ds, ax			; Setup the DS

	;mov al,3			; Set VGA mode 80x25
	;int 10h
	;cld

	%define	CHARS		3
	%define BACKGROUND 	7000h
	%define FONT		0400h
	mov [00h], word BACKGROUND | FONT | 'L'
	mov [02h], word BACKGROUND | FONT | 'O'
	mov [04h], word BACKGROUND | FONT | 'L'


	mov cx, (80*25 - CHARS)		; Clear the screen
	mov bx, CHARS*2
clear_scr:
	mov [bx], word BACKGROUND
	add bx, 02h
	loop clear_scr

x:
	jmp x				; while(1) {}

	times 512 - 2 - ($-$$) db 0	; Necessary padding
	db 055h, 0aah
