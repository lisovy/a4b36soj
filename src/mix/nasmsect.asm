org 7c00h

	jmp 0:setup
	
setup	xor ax,ax	;set segments to known values
	mov sp,7e00h
	mov ss,ax
	mov ds,ax
	mov al,3	;set VGA mode 80x25
	int 10h
	
	mov ax,0b800h
	mov es,ax
	
	cld
	mov si,text
	xor di,di	;position - 0, left upper corner
	mov ah,0fh	;color - bright white on black
	call print
	jmp $		;stop
	
place	stosw
print	lodsb		;get character
	and al,al
	jnz place	;and all non-NULL values write to VRAM together with attribute byte
	ret
	
text	db "BOOT sector compiled OK, using NASM",0

	times 1feh - ($ - $$) db 0	;necessary padding

	db 055h, 0aah
